import React, { Component } from "react";
import "./App.css";
import ProductCard from "./Components/ProductCard/ProductCard";
import Modal from "./Components/Modal/Modal"

class App extends Component {
  state = {
    cards: [],
    favCards: [],
    isCardFav: false,
    isModalOpen: false
  };

  componentDidMount() {
    fetch("./products.json")
      .then(res => res.json())
      .then(data => {
        this.setState({ cards: data });
      });
  }

  cardIsFav = () => {
  

    this.setState({
      isCardFav: !this.state.isCardFav 
    });
  };

  addToCart = () => {
    this.setState ({
    isModalOpen: !this.state.isModalOpen
    });
  }

  render() {
    const modal = <Modal bgrndClick = {this.closeModal}
    header = 'Do you want to register?'
    text = 'Please choose the social media to export register data'
    />
  
    const { cards } = this.state;
    const cardComponents = cards.map(card => (
      <ProductCard
      cardItem={card}
        title={card.name}
        icon={this.cardIsFav}
        img={card.url}
        text={card.text}
        addToCart = {this.addToCart}
      />
    ));

    return (
      <div className="App">
        <div className="container">
          <h1 className="title">LATEST ARRIVALS IN MUSICA</h1>
          <div className="cards-container">{cardComponents}</div>
        </div>
        {this.state.isModalOpen && modal}
      </div>
    );
  }  Ъ

}

export default App;
