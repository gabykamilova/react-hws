import React, { Component } from 'react';
import './ProductCard.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

class ProductCard extends Component {
    state = {
        favList: [],
        isFav: false
    }

addFav = (card) => {
    card.favorite = true;
}

changeIcon = (card) => {
    const storage = JSON.parse(localStorage.getItem('favorites'));
    console.log(storage)
    if(storage) {
        console.log('storage');
        const findItem = storage.find(item => item.name === card.name);
    
        console.log(findItem)
        if(findItem) {
            const findIndex = storage.indexOf(findItem);
            storage.splice(findIndex, 1);
            localStorage.setItem('favorites', JSON.stringify(storage));
        }
        else {
            storage.push(card);
            localStorage.setItem('favorites', JSON.stringify(storage));
        }
    } else {
        let test = [];
        test.push(card);
        localStorage.setItem('favorites', JSON.stringify(test))
    }
    this.setState({
        isFav: !this.state.isFav
    })
}
defaultIcon = (card) => {

    this.setState({
        isFav: card.favorite ? card.favourite
    })
}
  componentDidMount() {
    this.defaultIcon(this.props.cardItem)
  }
    render() {
    
        return (
            <div className = 'Container'>
                <div className="img" style = {{backgroundImage:`url(${this.props.img})`}}>{this.props.image}</div>
                <h3 className = 'Title'>{this.props.title}</h3>
                <p>{this.props.icon}</p>
                {this.props.addToFavourites}
                <p>{this.props.text}</p>
                <h2 className="Price">{this.props.price}</h2>
                <button className={this.state.isFav ? 'favs red':'favs'} onClick={() => this.changeIcon(this.props.cardItem)}><FontAwesomeIcon icon={faCoffee}  /></button>
                <button className="AddToCart" onclick = {this.props.addToCart}>ADD TO CART</button>
            </div>
        );
    }
}

export default ProductCard;