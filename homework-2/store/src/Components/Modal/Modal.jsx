import React, { Component } from 'react';
import './Modal.css'
import {Fragment} from 'react';


class Modal extends Component {

    render() {
    
        return (
            <Fragment>
            <div className = 'modal-wrapper' onClick = {this.props.bgrndClick}>
            <div 
            className = 'modal-style'
            >
        
                <header className = 'header-style'>
                {this.props.closeButton && <button className = 'cross-style' onClick = {this.props.closeModal}>x</button>}

                    {this.props.header}
                </header>
                <p className = 'main-text-style'>
                    {this.props.text}
                </p>
                <div className = 'modal-buttons'>{this.props.actions}</div>
            </div>
            </div>
            </Fragment>
        );
    }   
}


export default Modal;
