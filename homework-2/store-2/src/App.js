import React, {Component} from 'react';
import './App.css';
import Card from './components/card';

class App extends Component {
  state = {
    cards: [],
    favItems: []
    }

    componentDidMount() {
      fetch("./../products.json")
        .then(res => res.json())
        .then(data => {
          this.setState({ cards: data });
        });
    }


  render() { 
  
  
  const {cards} = this.state;
  const cardComponents = cards.map(card => (
    <Card 
    name = {card.name} 
    key = {card.articul} 
    price = {card.price} 
    url = {card.url} 
    articul = {card.articul} 
    text = {card.text} 
    isLiked= {this.state.isLiked} 
    onClick = {this.addToLocalStorage}
    toggleLike = {() => this.toggleLike(card)} />
  ))
    
    return (
          <div className="container">
        <div className="row" style = {{justifyContent: 'space-between'}}>
          {cardComponents}
        </div>
      </div>
    
      );
  }
}
 
export default App;
