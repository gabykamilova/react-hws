import React from 'react';
import {Component} from 'react';
import './App.css';
import Button from './Components/Button'
import Modal from './Components/Modal'
import FormButton from './Components/FormButton'

class App extends Component {
  state = {
    isModalOpen1: false,
    isModalOpen2: false,
  }

  showModal1 = () =>
  {
    this.setState ({
      isModalOpen1: !this.state.isModalOpen
    });
  }

  showModal2 = () =>
  {
    this.setState ({
      isModalOpen2: !this.state.isModalOpen
    });
  }

  closeModal = (e) => {
    if (e.target !== e.currentTarget) {
      return;
    }
    this.setState ({
      isModalOpen1: false,
      isModalOpen2: false,
    })
  }
 


render(){
  const modal1 = <Modal
  bgrndClick = {this.closeModal}
  header = 'Do you want to register?'
  text = 'Please choose the social media to export register data'
  actions = {[<FormButton title = 'Facebook' key = '1'/>,<FormButton title = 'Linkedin' key = '2'/>,<FormButton title = 'Google' key = '3'/>]}
  />
  const modal2 = <Modal
  bgrndClick = {this.closeModal}
  header = 'Do you want to login?'
  text = 'Please choose Yes or No'
  closeButton = 'true'
  closeModal = {this.closeModal}
  actions = {[<FormButton title = 'Yes' key = '3'/>,<FormButton title = 'No' key = '4'/>]}
  />

  return (
    <div className = 'App'>

      <Button text = 'Register' backgroundColor = 'lightblue' 
      handleClick = {this.showModal1}/>
      <Button text = 'Login' backgroundColor = 'lightyellow' 
      handleClick = {this.showModal2}/>
      {this.state.isModalOpen1 && modal1}
      {this.state.isModalOpen2 && modal2}

    </div>
  );
}
}


export default App;
