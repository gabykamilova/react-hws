import React, { Component } from 'react';
import './FormButton.css'


class FormButton extends Component {
    render() {
        return (
            <button className = 'form-button-style'>
                {this.props.title}
            </button>
        );
    }
}

export default FormButton;