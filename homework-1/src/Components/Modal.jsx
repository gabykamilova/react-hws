import React, { Component } from 'react';
import './Modal.css'
import {Fragment} from 'react';
import PropTypes from 'prop-types'

class Modal extends Component {

    render() {
    
        return (
            <Fragment>
            <div className = 'modal-wrapper' onClick = {this.props.bgrndClick}>
            <div 
            className = 'modal-style'
            >
        
                <header className = 'header-style'>
                {this.props.closeButton && <button className = 'cross-style' onClick = {this.props.closeModal}>x</button>}

                    {this.props.header}
                </header>
                <p className = 'main-text-style'>
                    {this.props.text}
                </p>
                <div className = 'modal-buttons'>{this.props.actions}</div>
            </div>
            </div>
            </Fragment>
        );
    }   
}
Modal.propTypes = {
    actions: PropTypes.array,
    text: PropTypes.string,
    bgrndClick: PropTypes.func,
    header: PropTypes.string
};

export default Modal;
