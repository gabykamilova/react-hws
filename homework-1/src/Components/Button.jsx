import React, { Component } from 'react';
import './Button.css'
import propTypes from 'prop-types'

class Button extends Component {
    render() {
        
        return (
            <button 
            type = 'button'
            className = 'button-style'
            style = {{backgroundColor: this.props.backgroundColor}}
            onClick = {this.props.handleClick}
            >
                {this.props.text}
            </button>
        );
    }
}
Button.propTypes = {
    handleClick: propTypes.func,
    text: propTypes.string
}


export default Button;