import React from "react";

const ItemInCart = props => {
  return (
    <li key={props.articul}>
      <div className="row">
        <div className="col">
          <img className="small-img" alt="#" src={props.url} />
        </div>
        <div className="col-7">
          <p className="title">{props.name} </p>
          <p>
            <b>Price: {props.price}</b>
          </p>
        </div>
        <div className="col">
          <p>
            <b>
              <button
                onClick={props.handleSubstractQuantity}
                className="quantity"
              >
                -
              </button>
              {props.quantity}
              <button className="quantity" onClick={props.handleAddQuantity}>
                +
              </button>
            </b>
          </p>
        </div>
        <div className="col">
          <button className="remove-button" onClick={props.handleRemove}>
            Remove
          </button>
        </div>
      </div>
    </li>
  );
};

export default ItemInCart;
