import React from "react";
import { connect } from "react-redux";
import { toggleFav } from "../actions/cartActions";

const Like = (props) => {
  let classes = "fa fa-star";

  if (!props.liked) classes += "-o";
  return (
    <i
      className={classes}
      onClick={() => props.toggleFav(props.articul)}
      aria-hidden="true"
      style={{ cursor: "pointer" }}
    />
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleFav: (articul) => {
      dispatch(toggleFav(articul));
    },
  };
};

export default connect(null, mapDispatchToProps)(Like);
