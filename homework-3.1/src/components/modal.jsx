import React from 'react';
import { Modal } from 'antd';

class Modal1 extends React.Component {

  render() {

    let modalText = !this.props.isProductInCart ? 'Product has been successfully added' : 'Product is already in the cart';



    return (
     
        <Modal
          title="Thank you"
          visible={this.props.visible}
          onOk={this.props.handleOk}
          onCancel={this.props.handleCancel}
        >
          <p>{modalText}</p>
         
        </Modal>
     
    );
  }
}

export default Modal1;