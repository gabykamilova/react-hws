import React, { Component } from "react";
import Like from "./like";
import Modal1 from "./modal";

class Card extends Component {
  constructor() {
    super();
    this.state = {
      isProductInCart: false,
      visible: false,
    };
  }

  openModal = () => {
    this.setState({ visible: true });
  };

  handleOk = (e) => {
    this.props.handleAddToCart();
    localStorage.setItem(`cart${this.props.articul}`, "in the cart");
    this.setState({
      isProductInCart: true,
      visible: false,
    });
  };

  handleCancel = (e) => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const modal = (
      <Modal1
        visible={this.state.visible}
        handleOk={this.handleOk}
        handleCancel={this.handleCancel}
        isProductInCart={this.state.isProductInCart}
      />
    );

    const productStatus = !this.state.isProductInCart ? "Add to Cart" : "Added";

    return (
      <React.Fragment>
        {modal}
        <div className="card mb-4 shadow-sm" style={{ margin: "8px" }}>
          <img alt="#" src={this.props.url}></img>
          <div className="card-body">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <p className="card-text">{this.props.name}</p>
              <span>
                <Like
                  articul={this.props.articul}
                  onClick={(e) => {
                    this.toggleLike(e);
                  }}
                  liked={this.props.isLiked}
                />
              </span>
            </div>
            <b>
              <p className="card-text">{this.props.price}</p>
            </b>
            <p className="card-text">{this.props.text}</p>
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button
                  type="button"
                  className="btn btn-sm btn-outline-secondary"
                  onClick={this.openModal}
                >
                  {productStatus}
                </button>
              </div>
              <small className="text-muted">{this.props.articul}</small>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Card;
