import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "./card";
import { addToCart, loadCatalog } from "../actions/cartActions";

class Catalog extends Component {
  addToCart = (articul) => {
    this.props.addToCart(articul);
  };

  componentDidMount() {
    if (!this.props.items.length) {
      fetch("./products.json")
        .then((res) => res.json())
        .then((items) => this.props.loadCatalog(items));
    }
  }

  render() {
    let cardComponents = this.props.items.map((item) => (
      <Card
        name={item.name}
        key={item.articul}
        price={item.price}
        url={item.url}
        articul={item.articul}
        text={item.text}
        onClick={this.addToLocalStorage}
        handleAddToCart={() => this.addToCart(item.articul)}
        isLiked={item.isFav}
      />
    ));

    return (
      <div className="container">
        <div className="row">{cardComponents}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.cart.items,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (articul) => {
      dispatch(addToCart(articul));
    },
    loadCatalog: (items) => {
      dispatch(loadCatalog(items));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
