export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_ITEM = "REMOVE_ITEM";
export const SUBSTRACT_QUANTITY = "SUBSTRACT_QUANTITY";
export const ADD_QUANTITY = "ADD_QUANTITY";
export const LOAD_CATALOG = "LOAD_CATALOG";
export const TOGGLE_FAV = "TOGGLE_FAV";
