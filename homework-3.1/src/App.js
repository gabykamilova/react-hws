import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Catalog from './components/catalog'
import Nav from './components/nav'
import Favourites from './components/favourites';
import Cart from './components/cart';




class App extends Component {
  render() { 
  
    return (
      <Router>
            <Nav className = 'nav-style'/>
            <Switch>
              <Route path = '/' exact component = {Catalog}/>
              <Route path = '/favourites' component = {Favourites}/>
              <Route path = '/cart' component = {Cart}/>
            </Switch>
      </Router>
      );
  }
}
 
export default App;
