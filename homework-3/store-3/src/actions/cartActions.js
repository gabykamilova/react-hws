import {
  ADD_TO_CART,
  REMOVE_ITEM,
  SUBSTRACT_QUANTITY,
  ADD_QUANTITY,
  TOGGLE_FAV
} from "./cart-actions";

export const addToCart = articul => {
  return {
    type: ADD_TO_CART,
    articul
  };
};

export const removeItem = articul => {
  return {
    type: REMOVE_ITEM,
    articul
  };
};
export const substractQuantity = articul => {
  return {
    type: SUBSTRACT_QUANTITY,
    articul
  };
};

export const addQuantity = articul => {
  return {
    type: ADD_QUANTITY,
    articul
  };
};

export const toggleFav = articul => {
  return {
    type: TOGGLE_FAV,
    articul
  };
};
