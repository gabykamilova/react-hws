import React from 'react';
import {NavLink} from 'react-router-dom'
import './nav.css';

const Nav = props => {
    return  (
       
            <nav>
                <ul className = 'menu'>
                    <li key = '123'><NavLink className = 'link-style' to = '/' exact activeStyle = {{color: 'lightblue'}}>Catalog</NavLink></li>
                    <li key = '456'><NavLink className = 'link-style' to = '/favourites' exact activeStyle = {{color: 'lightblue'}}><i className="fa fa-star" aria-hidden="true"></i> Favourites</NavLink></li>
                    <li key = '789'><NavLink className = 'link-style'to = '/cart' exact activeStyle = {{color: 'lightblue'}}> <i className="fa fa-shopping-cart" aria-hidden="true"></i> Cart</NavLink></li>
                </ul>
            </nav>
        
       
    )}

    export default Nav;
