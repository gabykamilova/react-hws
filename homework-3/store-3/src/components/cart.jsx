import React, { Component } from "react";
import { connect } from "react-redux";
import "./cart.css";
import { removeItem } from "../actions/cartActions";
import { addQuantity } from "../actions/cartActions";
import { substractQuantity } from "../actions/cartActions";
import ItemInCart from "./item-in-cart";

class Cart extends Component {
  handleAddQuantity = articul => {
    this.props.addQuantity(articul);
  };
  handleSubstractQuantity = articul => {
    this.props.substractQuantity(articul);
  };
  handleRemove = articul => {
    this.props.removeItem(articul);
  };

  render() {
    let itemsInCart = this.props.items.map(item => (
      <ItemInCart
        articul={item.articul}
        url={item.url}
        name={item.name}
        price={item.price}
        quantity={item.quantity}
        handleSubstractQuantity={() =>
          this.handleSubstractQuantity(item.articul)
        }
        handleAddQuantity={() => this.handleAddQuantity(item.articul)}
        handleRemove={() => this.handleRemove(item.articul)}
      />
    ));

    return (
      <div className="container">
        <div className="cart">
          <h5>Your Cart:</h5>
          <ul className="collection">{itemsInCart}</ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.cart.addedItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeItem: articul => {
      dispatch(removeItem(articul));
    },
    addQuantity: articul => {
      dispatch(addQuantity(articul));
    },
    substractQuantity: articul => {
      dispatch(substractQuantity(articul));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
