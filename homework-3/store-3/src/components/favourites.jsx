import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "./card";
import { toggleFav } from "../actions/cartActions";

class Favourites extends Component {
  render() {
    let addedProds = this.props.items.length ? (
      this.props.items.map(item => {
        return (
          <Card
            name={item.name}
            key={item.articul}
            price={item.price}
            url={item.url}
            articul={item.articul}
            text={item.text}
            onClick={this.addToLocalStorage}
            handleAddToCart={() => this.addToCart(item.articul)}
          />
        );
      })
    ) : (
      <p>The cart is empty</p>
    );
    return (
      <div className="container">
        <h5>Saved items:</h5>
        <div className="row" style={{ display: "flex" }}>
          {addedProds}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.fav.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFav: articul => {
      dispatch(toggleFav(articul));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favourites);
