import React, { Component } from "react";
import Like from "./like";
import Modal1 from "./modal";
import { connect } from "react-redux";
import { toggleFav } from "../actions/cartActions";

class Card extends Component {
  constructor() {
    super();
    this.state = {
      visible: false
    };
  }

  toggleFav = articul => {
    this.props.toggleFav(articul);
  };

  openModal = () => {
    this.setState({ visible: true });
  };

  handleOk = e => {
    this.props.handleAddToCart();
    localStorage.setItem(`cart${this.props.articul}`, "in the cart");
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  render() {
    const modal = (
      <Modal1
        visible={this.state.visible}
        handleOk={this.handleOk}
        handleCancel={this.handleCancel}
      />
    );

    const like = this.props.items.map(item => (
      <Like
        articul={item.articul}
        liked={item.isFav}
        toggleFav={() => this.toggleFav(item.articul)}
      />
    ));

    return (
      <React.Fragment>
        {modal}
        <div className="card mb-4 shadow-sm" style={{ margin: "8px" }}>
          <img alt="#" src={this.props.url}></img>
          <div className="card-body">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <p className="card-text">{this.props.name}</p>
              <span>{like}</span>
            </div>
            <b>
              <p className="card-text">{this.props.price}</p>
            </b>
            <p className="card-text">{this.props.text}</p>
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button
                  type="button"
                  className="btn btn-sm btn-outline-secondary"
                  onClick={this.openModal}
                >
                  Add to Cart
                </button>
              </div>
              <small className="text-muted">{this.props.articul}</small>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.fav.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFav: articul => {
      dispatch(toggleFav(articul));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);
