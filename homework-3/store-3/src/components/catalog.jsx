import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "./card";
import { addToCart } from "../actions/cartActions";

class Catalog extends Component {
  addToCart = articul => {
    this.props.addToCart(articul);
  };

  render() {
    let cardComponents = this.props.items.map(item => (
      <Card
        name={item.name}
        key={item.articul}
        price={item.price}
        url={item.url}
        articul={item.articul}
        text={item.text}
        onClick={this.addToLocalStorage}
        handleAddToCart={() => this.addToCart(item.articul)}
      />
    ));

    return (
      <div className="container">
        <div className="row">{cardComponents}</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.cart.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: articul => {
      dispatch(addToCart(articul));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
