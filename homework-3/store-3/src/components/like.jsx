import React from "react";

const Like = props => {
  let classes = "fa fa-star";
  if (!props.liked) classes += "-o";
  return (
    <i
      className={classes}
      onClick={e => props.toggleFav(props.articul)}
      aria-hidden="true"
      style={{ cursor: "pointer" }}
    />
  );
};

export default Like;
